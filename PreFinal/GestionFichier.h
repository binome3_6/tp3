/*************************************************************************
                           GestionFichier  -  description
                             -------------------
    début                : 12/12/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <GestionFichier> (fichier GestionFichier.h) ----------------
#if ! defined ( GESTIONFICHIER_H )
#define GESTIONFICHIER_H

//--------------------------------------------------- Interfaces utilisées
class Catalogue;
#include <string.h>

//------------------------------------------------------------- Constantes
const char delim = '/'; // Le caractère délimiteur dans le fichier de sauvegarde.
const string chaineNulle = "";
const int intNul = -1;
//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <GestionFichier>
//
//
//------------------------------------------------------------------------

class GestionFichier {
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques

    void importCatalogue();
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    void exportCatalogue();
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //


    //------------------------------------------------- Surcharge d'opérateurs
    GestionFichier & operator=(const GestionFichier & unGestionFichier);
    // Mode d'emploi :
    //
    // Contrat :
    //


    //-------------------------------------------- Constructeurs - destructeur
    GestionFichier(const GestionFichier & unGestionFichier);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    GestionFichier(Catalogue * catalogueParental);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~GestionFichier();
    // Mode d'emploi :
    //
    // Contrat :
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Méthodes protégées
    int saisieChoixMenu(string choixMenu);
    bool parsingCreationCatalogue(bool Simple, bool Compose, int debut, int fin, string VilleA, string VilleB);
    bool importAll();
    bool importIntervalle();
    bool importSelonVilleA();
    bool importSelonVilleB();
    bool importSimple();
    bool importCompose();
    bool exportAll(); 
    bool exportIntervalle();
    bool exportSelonVilleA();
    bool exportSelonVilleB();
    bool exportSimple();
    bool exportCompose();

    //----------------------------------------------------- Attributs protégés
    Catalogue * catalogueParent;

};

//-------------------------------- Autres définitions dépendantes de <GestionFichier>

#endif // GestionFichier_H

