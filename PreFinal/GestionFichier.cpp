/*************************************************************************
                           GestionFichier  -  description
                             -------------------
    début                : 12/12/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <GestionFichier> (fichier GestionFichier.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <typeinfo>

using namespace std;
//------------------------------------------------------ Include personnel
#include "GestionFichier.h"
#include "Catalogue.h"
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

void GestionFichier::importCatalogue()
{
#ifdef MAP
    cout << "Gestion fichier : méthode menu importation " << endl;
#endif

    string line;
    ifstream myfile("test.txt");

    if (myfile.is_open())
    {
#ifdef MAP
        cout << "Gestion fichier : méthode menu importation - Ouverture réussi du fichier." << endl;
#endif
        int nbTrajets = 0;
        int nbLigne = 0;

        while (getline(myfile, line))
        {
            if (nbLigne == 0)
            {
                nbTrajets = stoi(line);
            }
            nbLigne++;
#ifdef MAP
            cout << line << '\n';
#endif
        }
        cout << "> Le fichier comporte " << nbTrajets << " trajets et " << nbLigne << " lignes. Il est probablement " << (nbTrajets == nbLigne ? "valide." : "invalide.") << endl;

        myfile.close();
    }

    //On affiche le menu, et on récupère le choix utilisateur.
    int choix = this->saisieChoixMenu("importer");

    if (choix != 0)
    {
#ifdef MAP
        cout << "CATALOGUE : méthode menu importation - choix effectué" << choix << endl;
#endif
        bool answer = false;

        switch (choix)
        {
        case 1:
            // Importation complète
            cout << "Vous désirez effectuer une importation complète." << endl;
            importAll();
            break;
        case 2:
            // Tous les trajets simples
            cout << "Vous désirez effectuer une importation des trajets simples." << endl;
            importSimple();
            break;
        case 3:
            // Tous les trajets compsoés
            cout << "Vous désirez effectuer une importation des trajets composés." << endl;
            importCompose();
            break;
        case 4:
            // ville départ
            cout << "Vous désirez effectuer une importation selon la ville de départ." << endl;
            importSelonVilleA();
            break;
        case 5:
            // ville arrivée
            cout << "Vous désirez effectuer une importation selon la ville d'arrivée." << endl;
            importSelonVilleB();
            break;
        case 6:
            //certain nombre
            cout << "Vous désirez effectuer une importation selon un intervalle de trajets." << endl;
            importIntervalle();
            break;
        default:
            cout << "Votre choix ne correspond à rien de connu." << endl;
            break;
        }
    }
    else
    {
        cout << "Vous quittez le menu." << endl;
    }
}

bool GestionFichier::parsingCreationCatalogue(bool Simple, bool Compose, int debut, int fin, string VilleA, string VilleB)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu parsingCreationCatalogue - lancement" << endl;
#endif

    /*
    cout << "> Depuis quel fichier désirez-vous importer des données ? ";
    string nomFichier;
    cin >> nomFichier;
    
    ifstream fichier(nomFichier);
     */

    ifstream fichier("test.txt");
#ifdef MAP
    catalogueParent->Afficher();
#endif
    if (fichier)
    {
        //L'ouverture s'est bien passée, on peut donc lire
        string ligne; //Une variable pour stocker les lignes lues
        bool aUnProbleme = false;
        int nbLigne = 0;
        int nbTrajets = 0;
        bool estAjoutableParIndice = false;

        // =================== FIN des prétraitement, début du parcours ===================


        while (getline(fichier, ligne) && !aUnProbleme) //Tant qu'on n'est pas à la fin, on lit
        {
#ifdef MAP
            cout << "GestionFichier : méthode menu parsingCreationCatalogue - LECTURE => Voici la ligne N°" << nbLigne << " : " << ligne << endl;
#endif
            //Si c'est la première ligne, on récupère le nombre de trajets
            if (nbLigne == 0)
            {
                nbTrajets = stoi(ligne);
            }
            else
            {
                //On défini ce dont on a besoin pour construire la liste des arguments.
                stringstream streamLigne(ligne);
                string itemCourant;
                vector<string> tokens;

                while (getline(streamLigne, itemCourant, delim))
                {
                    tokens.push_back(itemCourant);
                }
#ifdef MAP
                cout << "GestionFichier : méthode menu parsingCreationCatalogue - On a parsé la ligne courante" << endl;
#endif
                //Toutes les variables dont on a besoin
                bool estTrajetSimple = false;
                bool arreteParsingCourant = false;

                string villeTerminaleB;
                string villeTMP;
                string villeTMPAncienne;
                MoyenDeTransport transportCourant;
                //On créé un pointeur sur notre trajetComposé courant.
                TrajetCompose* trajetComposeCourant;

                //Premier validateur : le numéro de ligne. Note : si il n'y a pas de demande sur le N° de ligne, on ne fait même pas le test. (car déjà à true)
                if ( (debut == intNul && fin == intNul) || (debut <= nbLigne && nbLigne < fin) ) // Le début est inclus, le dernier est exclu.
                {
                    //Si on a pas de 
                    estAjoutableParIndice = true;
#ifdef MAP
                    cout << "GestionFichier : méthode menu parsingCreationCatalogue - Cette ligne est ajoutable (soit parce que tout autorisé, soit parce que N° autorisé)" << endl;
#endif
                }
                else
                {
                    estAjoutableParIndice = false;
#ifdef MAP
                    cout << "GestionFichier : méthode menu parsingCreationCatalogue - Cette ligne n'est pas ajoutable (parce que N° pas autorisé)" << endl;
#endif  
                }


                //On va parcourir le tableau de parsing pour gérer les éléments un par un
                for (int unsigned i = 0; i < tokens.size() && !arreteParsingCourant && estAjoutableParIndice; i++)
                {
#ifdef MAP
                    cout << "GestionFichier : méthode menu parsingCreationCatalogue - Token actuel : " << tokens[i] << endl;
#endif

                    switch (i) //Suivant sur quelle case on est, on fait différents traitement
                    {
                    case 0: //On est sur la première case : le TYPE de trajet
                        if (tokens[i] == "1")
                        {
                            estTrajetSimple = true;
                            //C'est un simple. On veut les simple ? Si oui, on arrete pas le parsing, sinon, on arrete.
                            arreteParsingCourant = Simple ? false : true;


#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - Détection d'un trajet simple " << endl;
#endif
                        }
                        else
                        {
                            estTrajetSimple = false;
                            //C'est un composé. On veut les composé ? Si oui, on arrete pas le parsing, sinon, on arrete.
                            arreteParsingCourant = Compose ? false : true;
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - Détection d'un trajet composé. " << endl;
#endif
                        }
                        break;


                    case 1: //Pour le premier argument, On gère la sélection sur les villes
                        if (VilleA != chaineNulle && tokens[i] == VilleA)
                        {
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeA de ce trajet est celle recherchée: " << tokens[i] << endl;
#endif
                        }
                        else if (VilleA != chaineNulle) //Si on fait quand même une sélection sur la ville
                        {
                            //La ville n'est pas recherchée, alors qu'on fait une sélection sur les villes
                            arreteParsingCourant = true;
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeA de ce trajet n'est pas recherchée: " << tokens[i] << endl;
#endif
                        }
                        //On met la ville A, comme la première ville du premier trajet
                        villeTMP = tokens[i];
#ifdef MAP
                        cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeA de ce trajet est : " << tokens[i] << endl;
#endif
                        break;


                    case 2: //Pour le deuxième argument, On gère la sélection sur les villes

                        if (VilleB != chaineNulle && tokens[i] == VilleB)
                        {
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeB de ce trajet est celle recherchée: " << tokens[i] << endl;
#endif
                        }
                        else if (VilleB != chaineNulle) //Si on fait quand même une sélection sur la ville
                        {
                            //La ville n'est pas recherchée, alors qu'on fait une sélection sur les villes
                            arreteParsingCourant = true;
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeB de ce trajet n'est pas recherchée: " << tokens[i] << endl;
#endif
                        }
                        //On le garde pour la toute fin.
                        villeTerminaleB = tokens[i];
#ifdef MAP
                        cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeB de ce trajet est : " << tokens[i] << endl;
#endif
                        break;

                    case 3:
                        //On récupère le moyen de transport
                        transportCourant = static_cast<MoyenDeTransport> (stoi(tokens[i]));

                        //Si c'est un trajet simple, on a donc atteint "le bout"
                        if (estTrajetSimple)
                        {
                            //Ajout
                            catalogueParent->AjouterTrajetSimple(villeTMP, villeTerminaleB, transportCourant);

#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - C'est un trajet simple. Donc on l'a ajouté directement :  " << villeTMP << " à " << villeTerminaleB << " en " << transportCourant << endl;
#endif
                        }
                        else
                        { // C'est un trajet composé, il faut donc le préparer.
                            trajetComposeCourant = catalogueParent->AjouterTrajetCompose();
                        }

                        break;

                    default: //A partir du troisième argument, on gère ça automatiquement. On ne gère que le case du trajet composé à partir d'ici ! 

                        if (i % 2 == 1)
                        { // Si on est sur une case impaire, on repère un moyen
#ifdef MAP
                            cout << "GestionFichier : On ajoute le trajet suivant au trajet composé : " << villeTMPAncienne << " à " << villeTMP << " en " << transportCourant << endl;
#endif
                            trajetComposeCourant->AjouterTrajetSimple(villeTMPAncienne, villeTMP, transportCourant);

#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - On repère un moyen: " << tokens[i] << endl;
#endif
                            //On récupère le moyen de transport directement grâce à son numéro, soit pour le prochain tour, soit pour la finalisation
                            transportCourant = static_cast<MoyenDeTransport> (stoi(tokens[i]));

                            if (i == tokens.size() - 1) //Si on est sur la dernière case
                            {
#ifdef MAP
                                cout << "GestionFichier : On ajoute le trajet suivant (c'est le dernier!) au trajet composé : " << villeTMP << " à " << villeTerminaleB << " en " << transportCourant << endl;
#endif                                                                    
                                trajetComposeCourant->AjouterTrajetSimple(villeTMP, villeTerminaleB, transportCourant);
                            }

                        }
                        else if (i % 2 == 0)
                        { //Si on est sur une case paire, un repère une ville
#ifdef MAP
                            cout << "GestionFichier : On repère une ville: " << tokens[i] << endl;
#endif
                            //On stocke la ville courante.
                            villeTMPAncienne = villeTMP;
                            villeTMP = tokens[i];
                        }

                    }
                }
            }
#ifdef MAP
            cout << "GestionFichier : La ligne complète était : " << ligne << endl;
#endif
            //On rajoute les lignes
            nbLigne++;
        }

    }
    else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }
#ifdef MAP
    cout << "CATALOGUE : méthode menu parsingCreationCatalogue - fin" << endl;
#endif
#ifdef MAP
    catalogueParent->Afficher();
#endif
    return true;
}

bool GestionFichier::importAll()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu importAll - lancement" << endl;
#endif

    parsingCreationCatalogue(true, true, intNul, intNul, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "CATALOGUE : méthode menu importAll - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importSimple()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu importSimple - lancement" << endl;
#endif

    parsingCreationCatalogue(true, false, intNul, intNul, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "CATALOGUE : méthode menu importSimple - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importCompose()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu importSimple - lancement" << endl;
#endif

    parsingCreationCatalogue(false, true, intNul, intNul, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "CATALOGUE : méthode menu importSimple - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importIntervalle()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu importIntervalle - lancement" << endl;
#endif
    cout << "> Vous allez choisir l'intervalle de trajet que vous désirez importer." << endl;

    int a, b;
    cout << "> Borne minimale ?" << endl;
    cin >> a;
    cout << "> Borne maximale ?" << endl;
    cin >> b;

    parsingCreationCatalogue(true, true, a, b, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "CATALOGUE : méthode menu importIntervalle - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importSelonVilleA()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu importSelonVilleA - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville de départ, qui servira de condition à l'importation." << endl;

    string villeDepart;
    cout << "> Votre ville ?" << endl;
    cin >> villeDepart;

    parsingCreationCatalogue(true, true, intNul, intNul, villeDepart, chaineNulle);

#ifdef MAP
    cout << "CATALOGUE : méthode menu importSelonVilleA - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importSelonVilleB()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu importSelonVilleB - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville d'arrivée, qui servira de condition à l'importation" << endl;

    string villeArrive;
    cout << "> Votre ville ?" << endl;
    cin >> villeArrive;

    parsingCreationCatalogue(true, true, intNul, intNul, chaineNulle, villeArrive);

#ifdef MAP
    cout << "CATALOGUE : méthode menu importSelonVilleB - fin" << endl;
#endif
    return true;
}


// ======================= EXPORTATION =============================

bool GestionFichier::exportCatalogue()
{
#ifdef MAP
    cout << "GESTIONFICHIER : méthode 'Menu' d'un GestionFichier appelée " << endl;
#endif

    //On appelle la fonction d'affichage et de sélection du choix
    int choix = saisieChoixMenu("exporter");
    bool answer = false;

#ifdef MAP
    cout << "GESTIONFICHIER : méthode 'Menu' - choix effectué" << choix << endl;
#endif

    switch (choix)
    {
    case 1:
        // Exportation complète
        cout << "Vous désirez effectuer une exportation complète." << endl;
        answer = exportAll();
        break;
    case 2:
        // Tous les trajets simples
        cout << "Vous désirez effectuer une exportation des trajets simples." << endl;
        exportSimple();
        break;
    case 3:
        // Tous les trajets compsoés
        cout << "Vous désirez effectuer une exportation des trajets composés." << endl;
        exportCompose();
        break;
    case 4:
        // ville départ
        cout << "Vous désirez effectuer une exportation selon la ville de départ." << endl;
        exporttSelonVilleA();
        break;
    case 5:
        // ville arrivée
        cout << "Vous désirez effectuer une exportation selon la ville d'arrivée." << endl;
        exportSelonVilleB();
        break;
    case 6:
        //certain nombre
        cout << "Vous désirez effectuer une exportation selon un intervalle de trajets." << endl;
        exportIntervalle();
        break;
    default:
        cout << "Votre choix ne correspond à rien de connu." << endl;
        break;
        break;
    }

    // On veut donc quitter le programme
    cout << "> Votre saisie est terminée. " << endl;
    cout << "== Merci d'avoir utilisé notre gestionnaire de fichiers == " << endl;
    return answer;
}//----- Fin de Export

bool GestionFichier::exportAll()
// Fonction qui lexporte tous les trajets du catalogue dans le fichier
// Contrat : aucun
// Algorithme : aucun
{
    Catalogue catalogueUtiliseTMP = Catalogue();
    catalogueUtiliseTMP.Menu();

    bool reussite = false;
    ofstream fic("Sauvegarde.txt");

    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());

    cout << catalogueUtiliseTMP.GetNbTrajets() << "\n";
    for (int i = 0; i < catalogueUtiliseTMP.GetNbTrajets(); i++)
    {
#ifdef MAP /**attention le defmap écrira sur notre texte à l'appel de MAP**/
        cout << "Voilà le type du trajet etudie ";
#endif 
        int nbSousParcours = catalogueUtiliseTMP.collectionParcours[i]->GetNbSous_Parcours();
        cout << nbSousParcours << "/";
        cout << catalogueUtiliseTMP.collectionParcours[i]->GetVilleA() << "/";
        cout << catalogueUtiliseTMP.collectionParcours[i]->GetVilleB() << "/";
        if (nbSousParcours == 1)
        {
            cout << catalogueUtiliseTMP.collectionParcours[i]->GetNumeroMoyen();
        }
        else
        {
            //Trajet * TMP = catalogueUtiliseTMP.collectionParcours[i]->GetListeTrajets; 
            /**obtenir
            Trajet ** listeTrajets;**/

        }
        cout << "\n";

    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    return reussite;

}

bool GestionFichier::exportSimple()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu exportSimple - lancement" << endl;
#endif


#ifdef MAP
    cout << "CATALOGUE : méthode menu exportSimple - fin" << endl;
#endif
    return true;
}

bool GestionFichier::exportCompose()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu exportCompose - lancement" << endl;
#endif


#ifdef MAP
    cout << "CATALOGUE : méthode menu exportCompose - fin" << endl;
#endif
    return true;
}

bool GestionFichier::exportIntervalle()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu exportIntervalle - lancement" << endl;
#endif
    cout << "> Vous allez choisir l'intervalle de trajet que vous désirez exporter." << endl;

    int a, b;
    cout << "> Borne minimale ?" << endl;
    cin >> a;
    cout << "> Borne maximale ?" << endl;
    cin >> b;


#ifdef MAP
    cout << "CATALOGUE : méthode menu exportIntervalle - fin" << endl;
#endif
    return true;
}

bool GestionFichier::exportSelonVilleA()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu exportSelonVilleA - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville de départ, qui servira de condition à l'exportation." << endl;

    string villeDepart;
    cout << "> Votre ville ?" << endl;
    cin >> villeDepart;


#ifdef MAP
    cout << "CATALOGUE : méthode menu exportSelonVilleA - fin" << endl;
#endif
    return true;
}

bool GestionFichier::exportSelonVilleB()
{
#ifdef MAP
    cout << "CATALOGUE : méthode menu exportSelonVilleB - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville d'arrivée, qui servira de condition à l'exportation." << endl;

    string villeArrive;
    cout << "> Votre ville ?" << endl;
    cin >> villeArrive;


#ifdef MAP
    cout << "CATALOGUE : méthode menu exportSelonVilleB - fin" << endl;
#endif
    return true;
}

// type GestionFichier::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode


//------------------------------------------------- Surcharge d'opérateurs

GestionFichier & GestionFichier::operator=(const GestionFichier & unGestionFichier)
// Algorithme :
//
{
} //----- Fin de operator =


//-------------------------------------------- Constructeurs - destructeur

GestionFichier::GestionFichier(const GestionFichier & unGestionFichier)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <GestionFichier>" << endl;
#endif
} //----- Fin de GestionFichier (constructeur de copie)

GestionFichier::GestionFichier(Catalogue * catalogueParental)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <GestionFichier>" << endl;
#endif

    //On range le catalogue parent.
    this->catalogueParent = catalogueParental;

} //----- Fin de GestionFichier

GestionFichier::~GestionFichier()
{

}

//------------------------------------------------------------------ PRIVE

int GestionFichier::saisieChoixMenu(string choixMenu) //choix = exporter ou importer
// Fonction qui lance une IHM pour la saisie d'un choix du menu. Renvoi ledit choix.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GESTIONFICHIER : méthode 'saisieChoixMenu' d'un GestionFichier appelée " << endl;
#endif
    cout << "== Gestion de fichier : " + choixMenu + " ==" << endl;

    cout << "> Instructions : " << endl;
    cout << "> Saisissez '1' pour " + choixMenu + " tous les trajets du catalogue au fichier. " << endl;
    cout << "> Saisissez '2' pour " + choixMenu + " tous les trajets simples du catalogue au fichier. " << endl;
    cout << "> Saisissez '3' pour " + choixMenu + " tous les trajets composés du catalogue au fichier. " << endl;
    cout << "> Saisissez '4' pour " + choixMenu + " tous les trajets ayant une ville de depart spécifique. " << endl;
    cout << "> Saisissez '5' pour " + choixMenu + " tous les trajets ayant une ville d'arrivee spécifique. " << endl;
    cout << "> Saisissez '6' pour " + choixMenu + " un certain nombre de trajets du catalogue au fichier. " << endl;
    cout << "> Saisissez '0' pour quitter ce menu. " << endl;
    cout << "> Votre saisie : " << endl;

    // On récupère le choix utilisateur
    int choix;
    cin >> choix;

    return choix;
}//----- Fin de saisieChoixMenu



//----------------------------------------------------- Méthodes protégées

