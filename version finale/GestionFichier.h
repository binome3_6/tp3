/*************************************************************************
                           GestionFichier  -  description
                             -------------------
    début                : 12/12/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <GestionFichier> (fichier GestionFichier.h) ----------------
#if ! defined ( GESTIONFICHIER_H )
#define GESTIONFICHIER_H

//--------------------------------------------------- Interfaces utilisées
class Catalogue;
using namespace std;
#include <iostream>

//------------------------------------------------------------- Constantes
const char delim = '/'; // Le caractère délimiteur dans le fichier de sauvegarde.
const string chaineNulle = "";
const int intNul = -1;
const string nomFichierSave = "saveFile.txt";
//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <GestionFichier>
//
//
//------------------------------------------------------------------------

class GestionFichier {
    //----------------------------------------------------------------- PUBLIC

public:
    //----------------------------------------------------- Méthodes publiques

    void importCatalogue();
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //

    void exportCatalogue();
    // type Méthode ( liste des paramètres );
    // Mode d'emploi :
    //
    // Contrat :
    //


    //------------------------------------------------- Surcharge d'opérateurs
    GestionFichier & operator=(const GestionFichier & unGestionFichier);
    // Mode d'emploi :
    //
    // Contrat :
    //


    //-------------------------------------------- Constructeurs - destructeur
    GestionFichier(const GestionFichier & unGestionFichier);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    GestionFichier(Catalogue * catalogueParental);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~GestionFichier();
    // Mode d'emploi :
    //
    // Contrat :
    //

    //------------------------------------------------------------------ PRIVE

protected:
    //----------------------------------------------------- Méthodes protégées
    int saisieChoixMenu(string choixMenu);
    bool parsingCreationCatalogue(string nomFichier, bool Simple, bool Compose, int debut, int fin, string VilleA, string VilleB);
    bool importAll(string nomFichier);
    bool importIntervalle(string nomFichier);
    bool importSelonVilleA(string nomFichier);
    bool importSelonVilleB(string nomFichier);
    bool importSimple(string nomFichier);
    bool importCompose(string nomFichier);
    bool exportAll(string nomFichier); 
    bool exportIntervalle(string nomFichier);
    bool exportSelonVilleA(string nomFichier);
    bool exportSelonVilleB(string nomFichier);
    bool exportSimple(string nomFichier);
    bool exportCompose(string nomFichier);
    string normalise(string motNonNormalise);
    string getNomFichierImport();
    string getNomFichierExport();
    //----------------------------------------------------- Attributs protégés
    Catalogue * catalogueParent;

};

//-------------------------------- Autres définitions dépendantes de <GestionFichier>

#endif // GestionFichier_H

