/*************************************************************************
                           GestionFichier  -  description
                             -------------------
    début                : 12/12/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/

//---------- Réalisation de la classe <GestionFichier> (fichier GestionFichier.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <typeinfo>
using namespace std;
#include <cstring>
//------------------------------------------------------ Include personnel
#include "GestionFichier.h"
#include "Catalogue.h"
//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

void GestionFichier::importCatalogue()
{
#ifdef MAP
    cout << "Gestion fichier : méthode menu importation " << endl;
#endif

    string line;
    //On récupère le nom de fichier voulu par l'user.
    string nomFichierSaveLocal = getNomFichierImport();

    ifstream myfile(nomFichierSaveLocal);

    if (myfile.is_open())
    {
#ifdef MAP
        cout << "Gestion fichier : méthode menu importation - Ouverture réussi du fichier." << endl;
#endif
        int nbTrajets = 0;
        int nbLigne = 0;

        while (getline(myfile, line))
        {
            if (nbLigne == 0)
            {
                nbTrajets = stoi(line);
            }
            nbLigne++;
#ifdef MAP
            cout << line << '\n';
#endif
        }
        cout << "> Le fichier comporte " << nbTrajets << " trajets et " << nbLigne << " lignes. Il est probablement " << (nbTrajets + 1 == nbLigne ? "valide." : "invalide.") << endl;

        myfile.close();
    }



    //On affiche le menu, et on récupère le choix utilisateur.
    int choix = this->saisieChoixMenu("importer");

    if (choix != 0)
    {
#ifdef MAP
        cout << "CATALOGUE : méthode menu importation - choix effectué" << choix << endl;
#endif

        switch (choix)
        {
        case 1:
            // Importation complète
            cout << "Vous désirez effectuer une importation complète." << endl;
            importAll(nomFichierSaveLocal);
            break;
        case 2:
            // Tous les trajets simples
            cout << "Vous désirez effectuer une importation des trajets simples." << endl;
            importSimple(nomFichierSaveLocal);
            break;
        case 3:
            // Tous les trajets compsoés
            cout << "Vous désirez effectuer une importation des trajets composés." << endl;
            importCompose(nomFichierSaveLocal);
            break;
        case 4:
            // ville départ
            cout << "Vous désirez effectuer une importation selon la ville de départ." << endl;
            importSelonVilleA(nomFichierSaveLocal);
            break;
        case 5:
            // ville arrivée
            cout << "Vous désirez effectuer une importation selon la ville d'arrivée." << endl;
            importSelonVilleB(nomFichierSaveLocal);
            break;
        case 6:
            //certain nombre
            cout << "Vous désirez effectuer une importation selon un intervalle de trajets." << endl;
            importIntervalle(nomFichierSaveLocal);
            break;
        default:
            cout << "Votre choix ne correspond à rien de connu." << endl;
            break;
        }
    }
    else
    {
        cout << "Vous quittez le menu." << endl;
    }
}

bool GestionFichier::parsingCreationCatalogue(string nomFichier, bool Simple, bool Compose, int debut, int fin, string VilleA, string VilleB)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu parsingCreationCatalogue - lancement" << endl;
#endif

    /*
    cout << "> Depuis quel fichier désirez-vous importer des données ? ";
    string nomFichier;
    cin >> nomFichier;
    
    ifstream fichier(nomFichier);
     */

    ifstream fichier(nomFichier);
#ifdef MAP
    catalogueParent->Afficher();
#endif
    if (fichier)
    {
        //L'ouverture s'est bien passée, on peut donc lire
        string ligne; //Une variable pour stocker les lignes lues
        bool aUnProbleme = false;
        int nbLigne = 0;
        bool estAjoutableParIndice = false;
        int nbTrajets = 0;

        // =================== FIN des prétraitement, début du parcours ===================


        while (getline(fichier, ligne) && !aUnProbleme) //Tant qu'on n'est pas à la fin, on lit
        {
#ifdef MAP
            cout << "GestionFichier : méthode menu parsingCreationCatalogue - LECTURE => Voici la ligne N°" << nbLigne << " : " << ligne << endl;
#endif
            //Si c'est la première ligne, on récupère le nombre de trajets
            if (nbLigne == 0)
            {
                nbTrajets = stoi(ligne);
            }
            else
            {
                //On défini ce dont on a besoin pour construire la liste des arguments.
                stringstream streamLigne(ligne);
                string itemCourant;
                vector<string> tokens;

                while (getline(streamLigne, itemCourant, delim))
                {
                    tokens.push_back(itemCourant);
                }
#ifdef MAP
                cout << "GestionFichier : méthode menu parsingCreationCatalogue - On a parsé la ligne courante" << endl;
#endif
                //Toutes les variables dont on a besoin
                bool estTrajetSimple = false;
                bool arreteParsingCourant = false;

                string villeTerminaleB;
                string villeTMP;
                string villeTMPAncienne;
                MoyenDeTransport transportCourant;
                //On créé un pointeur sur notre trajetComposé courant.
                TrajetCompose* trajetComposeCourant;

                //Premier validateur : le numéro de ligne. Note : si il n'y a pas de demande sur le N° de ligne, on ne fait même pas le test. (car déjà à true)
                if ((debut == intNul && fin == intNul) || (debut <= nbLigne && nbLigne < fin)) // Le début est inclus, le dernier est exclu.
                {
                    //Si on a pas de 
                    estAjoutableParIndice = true;
#ifdef MAP
                    cout << "GestionFichier : méthode menu parsingCreationCatalogue - Cette ligne est ajoutable (soit parce que tout autorisé, soit parce que N° autorisé)" << endl;
#endif
                }
                else
                {
                    estAjoutableParIndice = false;
#ifdef MAP
                    cout << "GestionFichier : méthode menu parsingCreationCatalogue - Cette ligne n'est pas ajoutable (parce que N° pas autorisé)" << endl;
#endif  
                }


                //On va parcourir le tableau de parsing pour gérer les éléments un par un
                for (int unsigned i = 0; i < tokens.size() && !arreteParsingCourant && estAjoutableParIndice; i++)
                {
#ifdef MAP
                    cout << "GestionFichier : méthode menu parsingCreationCatalogue - Token actuel : " << tokens[i] << endl;
#endif

                    switch (i) //Suivant sur quelle case on est, on fait différents traitement
                    {
                    case 0: //On est sur la première case : le TYPE de trajet
                        if (tokens[i] == "1")
                        {
                            estTrajetSimple = true;
                            //C'est un simple. On veut les simple ? Si oui, on arrete pas le parsing, sinon, on arrete.
                            arreteParsingCourant = Simple ? false : true;


#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - Détection d'un trajet simple " << endl;
#endif
                        }
                        else
                        {
                            estTrajetSimple = false;
                            //C'est un composé. On veut les composé ? Si oui, on arrete pas le parsing, sinon, on arrete.
                            arreteParsingCourant = Compose ? false : true;
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - Détection d'un trajet composé. " << endl;
#endif
                        }
                        break;


                    case 1: //Pour le premier argument, On gère la sélection sur les villes
                        if (VilleA != chaineNulle && tokens[i] == VilleA)
                        {
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeA de ce trajet est celle recherchée: " << tokens[i] << endl;
#endif
                        }
                        else if (VilleA != chaineNulle) //Si on fait quand même une sélection sur la ville
                        {
                            //La ville n'est pas recherchée, alors qu'on fait une sélection sur les villes
                            arreteParsingCourant = true;
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeA de ce trajet n'est pas recherchée: " << tokens[i] << endl;
#endif
                        }
                        //On met la ville A, comme la première ville du premier trajet
                        villeTMP = tokens[i];
#ifdef MAP
                        cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeA de ce trajet est : " << tokens[i] << endl;
#endif
                        break;


                    case 2: //Pour le deuxième argument, On gère la sélection sur les villes

                        if (VilleB != chaineNulle && tokens[i] == VilleB)
                        {
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeB de ce trajet est celle recherchée: " << tokens[i] << endl;
#endif
                        }
                        else if (VilleB != chaineNulle) //Si on fait quand même une sélection sur la ville
                        {
                            //La ville n'est pas recherchée, alors qu'on fait une sélection sur les villes
                            arreteParsingCourant = true;
#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeB de ce trajet n'est pas recherchée: " << tokens[i] << endl;
#endif
                        }
                        //On le garde pour la toute fin.
                        villeTerminaleB = tokens[i];
#ifdef MAP
                        cout << "GestionFichier : méthode menu parsingCreationCatalogue - La villeB de ce trajet est : " << tokens[i] << endl;
#endif
                        break;

                    case 3:
                        //On récupère le moyen de transport
                        transportCourant = static_cast<MoyenDeTransport> (stoi(tokens[i]));

                        //Si c'est un trajet simple, on a donc atteint "le bout"
                        if (estTrajetSimple)
                        {
                            //Ajout
                            catalogueParent->AjouterTrajetSimple(villeTMP, villeTerminaleB, transportCourant);

#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - C'est un trajet simple. Donc on l'a ajouté directement :  " << villeTMP << " à " << villeTerminaleB << " en " << transportCourant << endl;
#endif
                        }
                        else
                        { // C'est un trajet composé, il faut donc le préparer.
                            trajetComposeCourant = catalogueParent->AjouterTrajetCompose();
                        }

                        break;

                    default: //A partir du troisième argument, on gère ça automatiquement. On ne gère que le case du trajet composé à partir d'ici ! 

                        if (i % 2 == 1)
                        { // Si on est sur une case impaire, on repère un moyen
#ifdef MAP
                            cout << "GestionFichier : On ajoute le trajet suivant au trajet composé : " << villeTMPAncienne << " à " << villeTMP << " en " << transportCourant << endl;
#endif
                            trajetComposeCourant->AjouterTrajetSimple(villeTMPAncienne, villeTMP, transportCourant);

#ifdef MAP
                            cout << "GestionFichier : méthode menu parsingCreationCatalogue - On repère un moyen: " << tokens[i] << endl;
#endif
                            //On récupère le moyen de transport directement grâce à son numéro, soit pour le prochain tour, soit pour la finalisation
                            transportCourant = static_cast<MoyenDeTransport> (stoi(tokens[i]));

                            if (i == tokens.size() - 1) //Si on est sur la dernière case
                            {
#ifdef MAP
                                cout << "GestionFichier : On ajoute le trajet suivant (c'est le dernier!) au trajet composé : " << villeTMP << " à " << villeTerminaleB << " en " << transportCourant << endl;
#endif                                                                    
                                trajetComposeCourant->AjouterTrajetSimple(villeTMP, villeTerminaleB, transportCourant);
                            }

                        }
                        else if (i % 2 == 0)
                        { //Si on est sur une case paire, un repère une ville
#ifdef MAP
                            cout << "GestionFichier : On repère une ville: " << tokens[i] << endl;
#endif
                            //On stocke la ville courante.
                            villeTMPAncienne = villeTMP;
                            villeTMP = tokens[i];
                        }

                    }
                }
            }
#ifdef MAP
            cout << "GestionFichier : La ligne complète était : " << ligne << endl;
#endif
            //On rajoute les lignes
            nbLigne++;
        }

    }
    else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }
#ifdef MAP
    cout << "GestionFichier : méthode menu parsingCreationCatalogue - fin" << endl;
#endif
#ifdef MAP
    catalogueParent->Afficher();
#endif
    return true;
}

bool GestionFichier::importAll(string nomFichier)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu importAll - lancement" << endl;
#endif

    parsingCreationCatalogue(nomFichier, true, true, intNul, intNul, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "GestionFichier : méthode menu importAll - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importSimple(string nomFichier)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu importSimple - lancement" << endl;
#endif

    parsingCreationCatalogue(nomFichier, true, false, intNul, intNul, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "GestionFichier : méthode menu importSimple - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importCompose(string nomFichier)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu importSimple - lancement" << endl;
#endif

    parsingCreationCatalogue(nomFichier, false, true, intNul, intNul, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "GestionFichier : méthode menu importSimple - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importIntervalle(string nomFichier)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu importIntervalle - lancement" << endl;
#endif
    cout << "> Vous allez choisir l'intervalle de trajet que vous désirez importer." << endl;

    int a, b;
    cout << "> Borne minimale ?" << endl;
    cin >> a;
    cout << "> Borne maximale ?" << endl;
    cin >> b;

    parsingCreationCatalogue(nomFichier, true, true, a, b, chaineNulle, chaineNulle);

#ifdef MAP
    cout << "GestionFichier : méthode menu importIntervalle - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importSelonVilleA(string nomFichier)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu importSelonVilleA - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville de départ, qui servira de condition à l'importation." << endl;

    string villeDepart;
    cout << "> Votre ville ?" << endl;
    cin >> villeDepart;

    parsingCreationCatalogue(nomFichier, true, true, intNul, intNul, villeDepart, chaineNulle);

#ifdef MAP
    cout << "GestionFichier : méthode menu importSelonVilleA - fin" << endl;
#endif
    return true;
}

bool GestionFichier::importSelonVilleB(string nomFichier)
{
#ifdef MAP
    cout << "GestionFichier : méthode menu importSelonVilleB - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville d'arrivée, qui servira de condition à l'importation" << endl;

    string villeArrive;
    cout << "> Votre ville ?" << endl;
    cin >> villeArrive;

    parsingCreationCatalogue(nomFichier, true, true, intNul, intNul, chaineNulle, villeArrive);

#ifdef MAP
    cout << "GestionFichier : méthode menu importSelonVilleB - fin" << endl;
#endif
    return true;
}

string GestionFichier::getNomFichierImport()
{
    string nomFichierTmp;
    bool estValide = false;

    while (!estValide)
    {
        cout << "> Quel fichier souhaitez vous importer ?" << endl;
        cin >> nomFichierTmp;
#ifdef MAP
        cout << "GestionFichier : méthode getNomFichierImport - fichier en entrée : " << nomFichierTmp << endl;
#endif
        // Tests pour vérifier que le fichier peut être ouvert.
        ifstream fileTest(nomFichierTmp);

        if (fileTest)
        {
            cout << "Le fichier spécifié est correct." << endl;
            estValide = true;
        }
        else if (fileTest.fail())
        {
            cout << "Erreur d'ouverture du fichier  : Fichier n'existe pas ?" << endl;
        }
        else
        {
            cout << "Erreur d'ouverture du fichier  : autres" << endl;

        }
    }

    return nomFichierTmp;
}
// ======================= EXPORTATION =============================

void GestionFichier::exportCatalogue()
{
#ifdef MAP
    cout << "GESTIONFICHIER : méthode 'Menu Exportation' d'un GestionFichier appelée " << endl;
#endif

    string nomFichierExportLocal = getNomFichierExport();

    //On appelle la fonction d'affichage et de sélection du choix
    int choix = saisieChoixMenu("exporter");

#ifdef MAP
    cout << "GESTIONFICHIER : méthode 'Menu' - choix effectué" << choix << endl;
#endif
    if (catalogueParent->getNbParcours() != 0)
    {
        switch (choix)
        {
        case 1:
            // Exportation complète
            cout << "Vous désirez effectuer une exportation complète." << endl;
            exportAll(nomFichierExportLocal);
            break;
        case 2:
            // Tous les trajets simples
            cout << "Vous désirez effectuer une exportation des trajets simples." << endl;
            exportSimple(nomFichierExportLocal);
            break;
        case 3:
            // Tous les trajets compsoés
            cout << "Vous désirez effectuer une exportation des trajets composés." << endl;
            exportCompose(nomFichierExportLocal);
            break;
        case 4:
            // ville départ
            cout << "Vous désirez effectuer une exportation selon la ville de départ." << endl;
            exportSelonVilleA(nomFichierExportLocal);
            break;
        case 5:
            // ville arrivée
            cout << "Vous désirez effectuer une exportation selon la ville d'arrivée." << endl;
            exportSelonVilleB(nomFichierExportLocal);
            break;
        case 6:
            //certain nombre
            cout << "Vous désirez effectuer une exportation selon un intervalle de trajets." << endl;
            exportIntervalle(nomFichierExportLocal);
            break;
        default:
            cout << "Votre choix ne correspond à rien de connu." << endl;
            break;
        }
    }
    else
    {
        cout << "> Il n'y a aucun trajets dans le catalogue. Exportation impossible." << endl;
    }


    // On veut donc quitter le programme
    cout << "> Votre saisie est terminée. " << endl;
    cout << "== Merci d'avoir utilisé notre gestionnaire de fichiers == " << endl;
}//----- Fin de Export

string GestionFichier::getNomFichierExport()
{
    string nomFichierTmp;
    bool estValide = false;

    while (!estValide)
    {
        cout << "> Dans quel fichier souhaitez vous exporter ?" << endl;
        cin >> nomFichierTmp;
#ifdef MAP
        cout << "GestionFichier : méthode getNomFichierExport - fichier en entrée : " << nomFichierTmp << endl;
#endif
        // Tests pour vérifier que le fichier peut être ouvert en lecture.
        ifstream fileTest(nomFichierTmp);

        //if (!fileTest.fail())
        if (fileTest)
        {
            cout << "Error: Fichier existe déjà ?" << endl; // Le fichier existe déjà, donc on recommence
        }
        else
        {
            cout << "Le fichier spécifié est correct." << endl;
            estValide = true;
        }
    }

    return nomFichierTmp;
}

bool GestionFichier::exportAll(string nomFichier)
// Fonction qui exporte tous les trajets du catalogue dans le fichier
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier : méthode menu exportAll - lancement" << endl;
#endif

    bool reussite = false;
    int nbTrajetsAjoutes = 0;
    ofstream fic(nomFichier);
    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());

    cout << catalogueParent->getNbParcours() << "\n";
    for (int i = 0; i < catalogueParent->getNbParcours(); i++)
    {
        catalogueParent->getCollectionParcours()[i]->PrintTrajets();
        nbTrajetsAjoutes++;
    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    reussite = true;

#ifdef MAP
    cout << "GestionFichier : méthode menu exportAll - " << nbTrajetsAjoutes << " trajets exportés" << endl;
    cout << "GestionFichier : méthode menu exportAll - fin" << endl;
#endif

    return reussite;
}

bool GestionFichier::exportSimple(string nomFichier)
// Fonction qui exporte tous les trajets simples du catalogue dans le fichier
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier : méthode menu exportSimple - lancement" << endl;
#endif

    bool reussite = false;
    ofstream fic(nomFichier);
    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());

    int nbTrajetSimple = 0;
    for (int i = 0; i < catalogueParent->getNbParcours(); i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetNbSous_Parcours() == 1)
        {
            nbTrajetSimple++;
        }
    }
    cout << nbTrajetSimple << "\n";
    int nbTrajetAjoute = nbTrajetSimple;
    for (int i = 0; nbTrajetSimple != 0; i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetNbSous_Parcours() == 1)
        {
            catalogueParent->getCollectionParcours()[i]->PrintTrajets();
            nbTrajetSimple--;
        }
    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    reussite = true;

#ifdef MAP
    cout << "GestionFichier : méthode menu exportSimple - " << nbTrajetAjoute << " trajets ajoutés" << endl;
    cout << "GestionFichier : méthode menu exportSimple - fin" << endl;
#endif
    return reussite;
}

bool GestionFichier::exportCompose(string nomFichier)
// Fonction qui exporte tous les trajets composés du catalogue dans le fichier
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier : méthode menu exportCompose - lancement" << endl;
#endif

    bool reussite = false;
    ofstream fic(nomFichier);
    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());

    int nbTrajetCompose = 0;
    for (int i = 0; i < catalogueParent->getNbParcours(); i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetNbSous_Parcours() != 1)
        {
            nbTrajetCompose++;
        }
    }
    cout << nbTrajetCompose << "\n";
    int nbTrajetAjoute = nbTrajetCompose;
    for (int i = 0; nbTrajetCompose != 0; i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetNbSous_Parcours() != 1)
        {
            catalogueParent->getCollectionParcours()[i]->PrintTrajets();
            nbTrajetCompose--;
        }
    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    reussite = true;


#ifdef MAP
    cout << "GestionFichier : méthode menu exportCompose - " << nbTrajetAjoute << " trajets ajoutés" << endl;
    cout << "GestionFichier : méthode menu exportCompose - fin" << endl;
#endif
    return reussite;
}

bool GestionFichier::exportIntervalle(string nomFichier)
// Fonction qui exporte les trajets de n à m (n inclus, m exclus) du catalogue dans le fichier
// Contrat : a correspondant à borne minimale est <= au b correspondant à la borne maximale
// Contrat : la borne maximale ne doit pas être plus grande que le nombre de parcours
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier : méthode menu exportIntervalle - lancement" << endl;
#endif
    cout << "> Vous allez choisir l'intervalle de trajet que vous désirez exporter." << endl;

    int a, b;
    cout << "> Borne minimale ?" << endl;
    cin >> a;
    cout << "> Borne maximale ?" << endl;
    cin >> b;

    if (a > b || b > catalogueParent->getNbParcours() || a == 0)
    {
#ifdef MAP
        cout << "GestionFichier : méthode menu exportIntervalle - erreur au niveau des bornes" << endl;
#endif  
        return false;
    }

    bool reussite = false;
    ofstream fic(nomFichier);
    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());
    int nbTrajetAjoute = 0;

    cout << b - a + 1 << "\n";
    for (int i = a; i <= b; i++)
    {
        catalogueParent->getCollectionParcours()[i - 1]->PrintTrajets();
        nbTrajetAjoute++;
    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    reussite = true;

#ifdef MAP
    cout << "GestionFichier : méthode menu exportIntervalle - " << nbTrajetAjoute << " trajets ajoutes" << endl;
    cout << "GestionFichier : méthode menu exportIntervalle - fin" << endl;
#endif
    return reussite;
}

bool GestionFichier::exportSelonVilleA(string nomFichier)
// Fonction qui exporte les trajets ayant une ville de départ spécifique 
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier : méthode menu exportSelonVilleA - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville de départ, qui servira de condition à l'exportation." << endl;
    cout << "> Votre ville ?" << endl;
    string villeDepart;
    cin>>villeDepart;

    bool reussite = false;
    ofstream fic(nomFichier);
    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());

    int nbReponse = 0;
    for (int i = 0; i < catalogueParent->getNbParcours(); i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetVilleA() == normalise(villeDepart))
        {
            nbReponse++;
        }
    }
    cout << nbReponse << "\n";
    int nbTrajetAjoute = nbReponse;
    for (int i = 0; nbReponse != 0; i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetVilleA() == normalise(villeDepart))
        {
            catalogueParent->getCollectionParcours()[i]->PrintTrajets();
            nbReponse--;
        }
    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    reussite = true;

#ifdef MAP
    cout << "GestionFichier : méthode menu exportSelonVilleA - " << nbTrajetAjoute << "trajets ajoutes " << endl;
    cout << "GestionFichier : méthode menu exportSelonVilleA - fin" << endl;
#endif
    return reussite;
}

bool GestionFichier::exportSelonVilleB(string nomFichier)
// Fonction qui exporte les trajets ayant une ville de départ spécifique 
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier : méthode menu exportSelonVilleB - lancement" << endl;
#endif
    cout << "> Vous allez choisir la ville d'arrivée, qui servira de condition à l'exportation." << endl;

    string villeArrive;
    cout << "> Votre ville ?" << endl;
    cin>>villeArrive;

    bool reussite = false;
    ofstream fic(nomFichier);
    streambuf *oldCoutBuffer = cout.rdbuf(fic.rdbuf());

    int nbReponse = 0;
    for (int i = 0; i < catalogueParent->getNbParcours(); i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetVilleB() == normalise(villeArrive))
        {
            nbReponse++;
        }

    }
    cout << nbReponse << "\n";
    int nbTrajetAjoute = nbReponse;
    for (int i = 0; nbReponse != 0; i++)
    {
        if (catalogueParent->getCollectionParcours()[i]->GetVilleB() == normalise(villeArrive))
        {
            catalogueParent->getCollectionParcours()[i]->PrintTrajets();
            nbReponse--;
        }
    }

    cout.rdbuf(oldCoutBuffer); //redefini le stream buffer de defaut
    fic.close();
    reussite = true;

#ifdef MAP
    cout << "GestionFichier : méthode menu exportSelonVilleB - " << nbTrajetAjoute << " trajets ajoutes" << endl;
    cout << "GestionFichier : méthode menu exportSelonVilleB - fin" << endl;
#endif
    return reussite;
}

//------------------------------------------------- Surcharge d'opérateurs
//-------------------------------------------- Constructeurs - destructeur

GestionFichier::GestionFichier(const GestionFichier & unGestionFichier)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <GestionFichier>" << endl;
#endif
} //----- Fin de GestionFichier (constructeur de copie)

GestionFichier::GestionFichier(Catalogue * catalogueParental)
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de <GestionFichier>" << endl;
#endif

    //On range le catalogue parent.
    this->catalogueParent = catalogueParental;

} //----- Fin de GestionFichier

GestionFichier::~GestionFichier()
{

}

//------------------------------------------------------------------ PRIVE

int GestionFichier::saisieChoixMenu(string choixMenu) //choix = exporter ou importer
// Fonction qui lance une IHM pour la saisie d'un choix du menu. Renvoi ledit choix.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GESTIONFICHIER : méthode 'saisieChoixMenu' d'un GestionFichier appelée " << endl;
#endif
    cout << "== Gestion de fichier : " + choixMenu + " ==" << endl;

    cout << "> Instructions : " << endl;
    cout << "> Saisissez '1' pour " + choixMenu + " tous les trajets du catalogue au fichier. " << endl;
    cout << "> Saisissez '2' pour " + choixMenu + " tous les trajets simples du catalogue au fichier. " << endl;
    cout << "> Saisissez '3' pour " + choixMenu + " tous les trajets composés du catalogue au fichier. " << endl;
    cout << "> Saisissez '4' pour " + choixMenu + " tous les trajets ayant une ville de depart spécifique. " << endl;
    cout << "> Saisissez '5' pour " + choixMenu + " tous les trajets ayant une ville d'arrivee spécifique. " << endl;
    cout << "> Saisissez '6' pour " + choixMenu + " un certain nombre de trajets du catalogue au fichier. " << endl;
    cout << "> Saisissez '0' pour quitter ce menu. " << endl;
    cout << "> Votre saisie : " << endl;

    int choix;
    bool estCorrect = false;

    while (!estCorrect)
    {
        // On récupère le choix utilisateur
        string answer;
        cin >> answer;
        try
        {
#ifdef MAP
            cout << "GestionFichier - saisieChoixMenu, stoi rend : " << stoi(answer) << endl;
#endif
            if (0 <= stoi(answer) && stoi(answer) <= 6)
            {
                choix = stoi(answer);
                estCorrect = true;
            }
            else
            {
                cout << "Entrée invalide. La valeur n'est pas dans le bon intervalle. Veuillez recommencer." << endl;
            }
        }
        catch (const std::exception & e)
        {
            cout << "Entrée invalide. Veuillez recommencer." << endl;
        }

    }

    return choix;
}//----- Fin de saisieChoixMenu

string GestionFichier::normalise(string motNonNormalise)
// Fonction qui normalise les entrées faites par l'utilisateur : Première lettre en majuscule, reste en minuscule.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "GestionFichier - Appel de la methode Normalise" << endl;
#endif

    //parcours de tous les caracteres composant motNonNormalise
    for (unsigned int i = 0; i < motNonNormalise.size(); i++)
    {
        char lettre = motNonNormalise[i];
        int codeASCIIlettre = (int) lettre; //traduction en code ASCII decimal
        if (codeASCIIlettre <= 122 && codeASCIIlettre >= 97 && i == 0) //caractere correspond a une minuscule et en debut du mot
        {
            codeASCIIlettre = codeASCIIlettre - 32; //debient une majuscule
#ifdef MAPV
            cout << "Code ascii premiere lettre modifiee" << endl;
#endif
        }
        if (codeASCIIlettre >= 65 && codeASCIIlettre <= 90 && i > 0) //caractere correspond a une majuscule
        {
            codeASCIIlettre = codeASCIIlettre + 32; //devient une minuscule
#ifdef MAPV
            cout << "Code ascii " << i + 1 << "eme lettre modifiee" << endl;
#endif
        }
        motNonNormalise[i] = (char) codeASCIIlettre;
    }
#ifdef MAP
    cout << "GestionFichier - Appel de la methode Normalise finie" << endl;
    cout << "GestionFichier - Appel de la methode Normalise - valeur rendue finale : " << motNonNormalise << endl;
#endif
    return motNonNormalise;
}//----- Fin de Normalise


//----------------------------------------------------- Méthodes protégées

