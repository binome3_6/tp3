/*************************************************************************
                           TrajetSimple  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <TrajetSimple> (fichier TrajetSimple.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>
//------------------------------------------------------ Include personnel
#include "TrajetSimple.h"
//------------------------------------------------------------- Constantes
//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques

const char * TrajetSimple::GetVilleA()
// On récupère la villeA du trajet simple courant. En somme, un Getter.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cerr << "TRAJET SIMPLE : méthode 'getVilleA' d'un TRAJET SIMPLE appelée " << endl;
    cerr << villeDepart << endl;
#endif
    return villeDepart;
}

const char * TrajetSimple::GetVilleB()
// On récupère la villeB du trajet simple courant. En somme, un Getter.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cerr << "TRAJET SIMPLE : méthode 'getVilleB' d'un TRAJET SIMPLE appelée " << endl;
    cerr << villeDestination << endl;
#endif
    return villeDestination;
}

MoyenDeTransport TrajetSimple::GetMoyen()
// On récupère le moyen de transport du trajet simple courant. En somme, un Getter.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "TRAJET SIMPLE : méthode 'getMoyen' d'un TRAJET SIMPLE appelée " << endl;
    afficherEnumMoyen(moyenTransport);
#endif

    return moyenTransport;
}

int TrajetSimple::GetNbSous_Parcours()
// récupére le nombre de sous-trajets composants le trajet etudie (ici 1 car trajet simple).
// Définition vide.
// Contrat : aucun
{
    return nbTrajets;
    ;
}

int TrajetSimple::GetNumeroMoyen()
{
    return moyenTransport;
}

void TrajetSimple::PrintTrajets()
{
#ifdef MAP
    cerr << "APPEL: méthode 'PrintTrajets' d'un TRAJET SIMPLE appelée " << endl;
    //Désactivé car sinon écrit dans le fichier courant ! 
#endif 

    cout << nbTrajets << "/";
    cout << this->GetVilleA() << "/";
    cout << this->GetVilleB() << "/";
    cout << this->GetNumeroMoyen() << "\n";
}

void TrajetSimple::Afficher()
// Affichage des caractéristique d'un trajet simple. L'affichage ne comporte pas de saut de ligne
// Pour pouvour être utilisé lors d'affichage plus conséquents.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "TRAJET SIMPLE : méthode 'Affichage' d'un TRAJET SIMPLE appelée " << endl;
#endif
    cout << "de " << villeDepart << " à " << villeDestination << " en ";

    afficherEnumMoyen(moyenTransport);
}

//------------------------------------------------- Surcharge d'opérateurs
//-------------------------------------------- Constructeurs - destructeur

TrajetSimple::TrajetSimple(const TrajetSimple & unTrajetSimple) : Trajet(unTrajetSimple)
// Constructeur de copie d'un trajet simple.
// Implémenté, mais ne sert probablement pas.
// Contrat : aucun
// Algorithme : aucun
{
    // On initialise et on copie les valeurs
    this->villeDepart = new char[strlen(unTrajetSimple.villeDepart) + 1]; // Caractère '\0'
    strcpy(this->villeDepart, unTrajetSimple.villeDepart);

    this->villeDestination = new char[strlen(unTrajetSimple.villeDestination) + 1]; // Caractère '\0'
    strcpy(this->villeDestination, unTrajetSimple.villeDestination);

    this->moyenTransport = unTrajetSimple.moyenTransport;

#ifdef MAP
    cout << "Appel au constructeur de copie de <TrajetSimple>" << endl;
#endif
} //----- Fin de TrajetSimple (constructeur de copie)

TrajetSimple::TrajetSimple(const char * villeDepart, const char * villeArrivee, const MoyenDeTransport transport) : Trajet()
// Constructeur d'un trajet simple. 
// La villeDepart correspond à la ville de départ qui sera allouée à ce trajet.
// La ville arrivée corespond à la ville d'arrivée qui sera allouée à ce trajet.
// Le moyen de transport est le moyen de transport utilisé pour ce trajet, entre les villes de départ et d'arrivée.
// Contrat : aucun
// Algorithme : aucun
{
    // On initialise les attributs d'un Trajet Simple
    this->villeDepart = new char[strlen(villeDepart) + 1]; // Caractère '\0'
    strcpy(this->villeDepart, villeDepart);

    this->villeDestination = new char[strlen(villeArrivee) + 1]; // Caractère '\0'
    strcpy(this->villeDestination, villeArrivee);

    this->moyenTransport = transport;

    this->nbTrajets = 1;

#ifdef MAP
    cout << "Appel au constructeur de <TrajetSimple>" << endl;
#endif
} //----- Fin de TrajetSimple

TrajetSimple::~TrajetSimple()
// Destructeur de trajet simple, libération des pointeurs de ville.
// Contrat : aucun
// Algorithme : aucun
{
    // On dés-salloue les pointeurs de l'instance
    delete[] this->villeDepart;
    delete[] this->villeDestination;

#ifdef MAP
    cout << "Appel au destructeur de <TrajetSimple>" << endl;
#endif
} //----- Fin de ~TrajetSimple

//------------------------------------------------------------------ PRIVE
//----------------------------------------------------- Méthodes protégées
//------------------------------------------------------- Méthodes privées

void TrajetSimple::afficherEnumMoyen(MoyenDeTransport ceMoyen)
// Utilisé par plusieurs méthode, permet simplement d'afficher un moyen,
// comme le code utilise une constante, pas évidente.
// Contrat : aucun
// Algorithme : aucun
{
    cout << MOYEN_DE_TRANSPORT_CHAINE[ceMoyen];
}
