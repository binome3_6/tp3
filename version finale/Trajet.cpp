/*************************************************************************
                           TRAJET  -  description
                             -------------------
    début                : 14/11/2016
    copyright            : (C) 2016 par FALCONIERI & LACHAT
    e-mail               : vincent.falconieri@insa-lyon.fr & laetita.lachat@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <TRAJET> (fichier TRAJET.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>
//------------------------------------------------------ Include personnel
#include "Trajet.h"
//------------------------------------------------------------- Constantes
//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques

const char * Trajet::GetVilleA()
// Permet de suivre si cette méthode est appellée.
// On récupère la villeA du trajet courant. En somme, un Getter.
// Ici, elle ne joue le rôle que de méthode à implémenter pour les descendants de la classe.
// renvoit une ville vide.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "ERREUR : méthode 'getVilleA' d'un TRAJET appelée " << endl;
#endif
    return VILLE_VIDE;
}

const char * Trajet::GetVilleB()
// Permet de suivre si cette méthode est appellée.
// On récupère la villeB du trajet courant. En somme, un Getter.
// Ici, elle ne joue le rôle que de méthode à implémenter pour les descendants de la classe.
// renvoit une ville vide.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "ERREUR : méthode 'getVilleB' d'un TRAJET appelée " << endl;
#endif
    return VILLE_VIDE;
}

int Trajet::GetNbSous_Parcours()
// Permet de suivre si cette méthode est appellée.
// On doit récupèrer le nombre de parcours qui compose un trajet (pour un trajet simple : 1, pour un trajet compose : >=2)
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "ERREUR : méthode 'GetNbSous_Parcours' d'un TRAJET appelée " << endl;
#endif 
    return 0;
}

int Trajet::GetNumeroMoyen()
// Permet de suivre si cette méthode est appellée.
// On doit récupèrer le moyen utilisé pour un trajet simple
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "ERREUR : méthode 'GetNumeroMoyen' d'un TRAJET appelée " << endl;
#endif 
    return 0;
}

void Trajet::PrintTrajets()
{
#ifdef MAP
    cout << "ERREUR : méthode 'GetListeTrajets' d'un TRAJET appelée " << endl;
#endif  
}

void Trajet::Afficher()
// Permet de suivre si cette méthode est appellée.
// ne renvoit aucun affichage
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "ERREUR : méthode 'Afficher' d'un TRAJET appelée " << endl;
#endif
}

//------------------------------------------------- Surcharge d'opérateurs
//-------------------------------------------- Constructeurs - destructeur

Trajet::Trajet(const Trajet & unTrajet)
// Constructeur de copie d'un trajet .
// Permet de suivre si cette méthode est appellée.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Trajet>" << endl;
#endif
} //----- Fin de Trajet (constructeur de copie)

Trajet::Trajet()
// Constructeur d'un trajet .
// Permet de suivre si cette méthode est appellée.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "Appel au constructeur de <Trajet>" << endl;
#endif
} //----- Fin de Trajet

Trajet::~Trajet()
// Destructeur d'un trajet .
// Permet de suivre si cette méthode est appellée.
// Contrat : aucun
// Algorithme : aucun
{
#ifdef MAP
    cout << "Appel au destructeur de <Trajet>" << endl;
#endif
} //----- Fin de ~Trajet
//------------------------------------------------------------------ PRIVE
//----------------------------------------------------- Méthodes protégées
//------------------------------------------------------- Méthodes privées
